<?php
function select_records(){
	// Create connection
	$conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD, DB);
	// Check connection
	if ($conn->connect_error) {
	    return false;
	} 

	$sql = "SELECT title, id FROM movies ORDER BY title";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		return($result);
	} else {
	   return false;
	}
	$conn->close();
}

function info($id){
	// Create connection
	$conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD, DB);
	// Check connection
	if ($conn->connect_error) {
	    return false;
	} 

	$sql = "SELECT title, id, format, year FROM movies WHERE id = '$id' LIMIT 1";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$sql = "SELECT name FROM actors WHERE movi_id = '$id'";
		$actors = $conn->query($sql);
		return(array($result, $actors));
	} else {
	   return false;
	}
	$conn->close();
}

function addRecord(string$t,string $y,string $f,array $a){

	//print_r($a);
	$conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD, DB);
	// Check connection
	if ($conn->connect_error) {
	    return false;
	}

	$sql = "INSERT INTO movies (title, year, format)
	VALUES ('$t', '$y', '$f')";

	if ($conn->query($sql) === TRUE) {
   		$mov_id = $conn->insert_id;
   		foreach ($a as $key => $value) {
			$sql = "INSERT INTO actors (name, movi_id)
			VALUES ('$value', '$mov_id')";
			$conn->query($sql);
   		}
   		return true;
	} else {
	   return false;
	}

	$conn->close();
}

function remove(int $id){
	$conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD, DB);
	// Check connection
	if ($conn->connect_error) {
	    return false;
	}

	$sql = "DELETE FROM movies WHERE id='$id'";

	if ($conn->query($sql) === TRUE) {
   		return true;
	} else {
	   return false;
	}

	$conn->close();
}

function search_by_title(array $s,  string $s_s){
	// Create connection
	$conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD, DB);
	// Check connection
	if ($conn->connect_error) {
	    return false;
	}

	$str = "'%$s_s%' ";
	foreach ($s as $k => $v) {
		$str .= "OR title LIKE '%$v%'";
	}
	$sql = "SELECT title, id FROM movies WHERE title  LIKE $str ORDER BY title";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		return($result);
	} else {
	   return false;
	}
	$conn->close();
}

function search_by_artist(string $s){
	$conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD, DB);
	// Check connection
	if ($conn->connect_error) {
	    return false;
	}
	$sql = "SELECT movi_id FROM actors WHERE name LIKE '%$s%'";
	$result2 = $conn->query($sql);
	if ($result2->num_rows > 0) {
		while($row = $result2->fetch_assoc()) {
		$id_f = $row["movi_id"];
		$sql = "SELECT title, id FROM movies WHERE id='$id_f'";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
				return $result;
			}
    	}
	} else {
	   return false;
	}
	$conn->close();
}

