<?php include "header.tpl.php"; ?>
<?php if ($record): ?>
<?php while($row = $record->fetch_assoc()): ?>
	<h2><?php echo $row["title"] ?></h2>
	<p><?php echo $row["format"] ?></p>
	<p><?php echo $row["year"] ?></p>

	<h3>Actors</h3>
	<?php while($row = $actors->fetch_assoc()): ?>
		<p><?php echo $row["name"] ?></p>
	<?php endwhile ?>
<?php endwhile ?>
<?php else: ?>
	<h3>Record is not found</h3>
<?php endif ?>
<?php include "footer.tpl.php"; ?>