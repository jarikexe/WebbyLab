<?php include "header.tpl.php"; ?>

<form action="?act=search" method="post">
	<div class="row">
		<div class="form-group col-8">
	    	<input required="" type="text" name="title" class="form-control" placeholder="Title / actor">
		</div>
		<div class="form-group col-2"> 
	   		<button name="movei" type="submit" class="btn">Search by title</button>
		</div>
	    <div class="form-group col-2"> 
	   		<button name="actor" type="submit" class="btn">Search by author</button>
		</div>
	</div>


</form>
<div class="col-12">
<?php if ($records): ?>
<?php while($row = $records->fetch_assoc()): ?>
	<a href="?act=info&id=<?php echo $row["id"] ?>"><h2><?php echo $row["title"] ?></h2></a>
<?php endwhile ?>
<?php else: ?>
	<h3>No records found yet</h3>
<?php endif ?>
<?php include "footer.tpl.php"; ?>
</div>