<?php include "header.tpl.php"; ?>
<h1 class="text-center">Add/Edit your movies</h1>

<h3>Add new movie</h3>
<form action="?act=new_movie" method="POST">
<div class="row">
	<div class="form-group col-3">
    <input required="" type="text" name="title" class="form-control" placeholder="Title">
  </div>
  <div class="form-group col-2">
    <input required="" type="number" name="year" class="form-control" placeholder="Year">
  </div>
  <div class="form-group col-2">
  	<select name="format"  class="col-12 custom-select">
  		<option value="VHS">VHS</option>
  		<option value="DVD" selected>DVD</option>
  		<option value="Blu-Ray">Blu-Ray</option>
  	</select>
  </div>
  <div class="form-group col-5">
    <textarea rows='5' required="" name="actor" class="form-control"> </textarea>
    Enter Actors of film separated by comma<br>
    Exmp: <em> First Name Last Name, First Name Last Name</em>
  </div>

  <div class="form-group col-3">
    <input type="submit" class="btn btn-success" value="Add this movie">
  </div>
  </div>
</form>

<h3>Edit movies</h3>


<?php if ($records): ?>
<?php while($row = $records->fetch_assoc()): ?>
	<h2>
		<a href="?act=remove&id=<?php echo $row[id] ?>"><span class="oi oi-circle-x text-danger" title="Remove the movie"></span></a>
		<?php echo $row["title"] ?>
	</h2>
<?php endwhile ?>
<?php else: ?>
<h3>No records found yet</h3>
<?php endif ?>




<?php include "footer.tpl.php"; ?>