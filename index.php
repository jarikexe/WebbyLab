<?php 
require("cnf.php");
$act = $_GET["act"];

switch ($act) {
	case 'list':
		$records = select_records();
		require("tpl/list.tpl.php");
		break;
	case 'info':
		$id = $_GET['id'];
		$record = info($id)[0];
		$actors = info($id)[1];

		require("tpl/info.tpl.php");
		break;
	case 'edit':
		$records = select_records();
		require("tpl/add.tpl.php");
		break;
	case 'new_movie':
		$title = htmlspecialchars($_POST["title"]);
		$year = htmlspecialchars($_POST["year"]);
		$format = htmlspecialchars($_POST["format"]);
		$actors = htmlspecialchars($_POST["actor"]);

		$actors = explode( ',', $actors);
		if(addRecord($title, $year,$format,$actors)){
			header("Location: ?act=edit");
		}
		else echo "Something is wrong contact administration";
		break;
	case 'remove':
		$id = htmlspecialchars($_GET["id"]);
		if(remove($id)) header("Location: ?act=edit");
		else echo "Something is wrong contact administration";
		break;
	case 'search':
		$search = htmlspecialchars($_POST['title']);
		$search = str_replace("'", "", $search);
		$search_arr = explode(" ", $search);

		if(isset($_POST["movei"])) $records = search_by_title($search_arr, $search);
		if(isset($_POST["actor"])) $records = search_by_artist($search);
		//var_dump($records);
		require("tpl/list.tpl.php");
	break;
	default:
		header("Location: ?act=list");
		break;
}

?>